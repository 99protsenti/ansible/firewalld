Firewalld
=========

This role installs and provides basic configuration for the firewalld firewall daemon.

Requirements
------------

This role can be used with any Ubuntu/Debian based system starting at oldstable branch. It also has support for CentOS 7, but haven't been tested in any other RedHat-like environment.

Role Variables
--------------

### `firewalld_public_interface`:

**Type**: `string`

**Default**: `eth0`

**Description**: Via this variable you can specify your public network interface. Only one supported at this moment.

### `firewalld_allowed_services`:

**Type**: `list`

**Default**: `[]`

**Description**: Allows addition of services you do not wish to cover in your playbook, like standalone http/https server.

Role Handlers
-------------

### `Restart firewalld`:

**Description**: Restarts firewalld service.

### `Reload firewalld`:

**Description**: Reloads firewalld service.

Dependencies
------------

At this time, this role requires no dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```ansible
hosts: any
roles:
  - role: firewalld
    variables:
      firewalld_public_interface: enp1
      firewalld_allowed_services:
        - http
        - https
```

License
-------

BSD
